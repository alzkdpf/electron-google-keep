# taskworld-electron

**웹페이지를 앱으로 실행

electron을 이용하여 어플리케이션으로 빌드


## 이용법

```bash
# electron-packager 설치
npm install electron-packager -g
# 패키지 설치
npm install

#빌드
electron-packager <sourcedir> <appname> --platform=<platform> --arch=<arch> [optional flags...]
#실제 예제
electron-packager ./ google-keep --platform=darwin --arch=x64 --icon=icon/app-icon.icns
electron-packager ./ google-keep --platform=darwin --arch=x64
```

## 참조

- [electron-packager](https://github.com/electron-userland/electron-packager) - 일렉트론 패키지 저장소
- [electron](https://electronjs.org/) - 일렉트론 페이지
